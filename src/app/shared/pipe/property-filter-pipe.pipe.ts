import { Pipe, PipeTransform } from '@angular/core';
import { Property } from '../models/config-property';

@Pipe({
  name: 'propertyFilterPipe'
})
export class PropertyFilterPipe implements PipeTransform {

  transform(value: any[], filterInput: any): unknown {
    if (filterInput) {
      return value.filter(property => {
        return property.propertyName.toLocaleLowerCase().includes(filterInput.toLocaleLowerCase());
      });
    } else {
      return value;
    }
  }

}
