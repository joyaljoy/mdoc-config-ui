import { Property } from './config-property';

export interface config {
    
    // fileName: string;
    propertySource: string;
    application: string;
    profile: string;
    properties: Property[];
}