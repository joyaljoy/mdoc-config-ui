import { Component, OnInit } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { Router } from '@angular/router';

@Component({
  selector: 'app-config-select',
  templateUrl: './config-select.component.html',
  styleUrls: ['./config-select.component.scss']
})
export class ConfigSelectComponent implements OnInit {

  configs: any[] = [];
  selectedConfig: string = "";
  selectedApp: string = "";
  selectedProfile: string = "";
  selectedLabel: string = "";


  constructor(private router: Router) { }

  ngOnInit(): void {
    // call backend api to get list of app/prod/label array

    // dummy value
    this.configs = JSON.parse('[{"app":"local","profiles":["dev","prod","uat"],"labels":["master","develop"]},' +
      '{"app":"meona","profiles":["prod","uat"],"labels":["master"]}]');
  }

  selectedValue(property: string, event: MatSelectChange) {
    if (property == 'app')
      this.selectedApp = event.value;
    else if (property == 'profile')
      this.selectedProfile = event.value;
    else if (property == 'label')
      this.selectedLabel = event.value;
  }

  callConfig() {
    this.router.navigate(['/edit-properties']);
    console.log("app:" + this.selectedApp + ", profile:" + this.selectedProfile + ", label:" + this.selectedLabel);
  }

}
