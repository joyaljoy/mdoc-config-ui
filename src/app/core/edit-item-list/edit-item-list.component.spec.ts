import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditItemListComponent } from './edit-item-list.component';

describe('EditItemListComponent', () => {
  let component: EditItemListComponent;
  let fixture: ComponentFixture<EditItemListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditItemListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditItemListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
