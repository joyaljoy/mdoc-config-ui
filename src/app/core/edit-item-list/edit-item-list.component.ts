import { Component, OnInit } from '@angular/core';
import { config } from 'src/app/shared/models/config';
import { Property } from 'src/app/shared/models/config-property';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-edit-item-list',
  templateUrl: './edit-item-list.component.html',
  styleUrls: ['./edit-item-list.component.scss']
})
export class EditItemListComponent implements OnInit {

  tabs: string[] = [];
  configs: config[] = [];
  updatedConfigs: config[] = [];
  // configProperties: Property[] = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    // fill this array with the api response of backend call
    // this.tabs = ['application.properties', 'webconfig.json', 'theme.css'];
    this.getDummyJson();
    // this.configs.forEach((config) => { this.tabs.push(config.fileName) });
  }

  getDummyJson() {
    // write actual rest call here
    // var dummyJson = '[{"fileName":"application.properties","properties":[{"propertyName":"application.property.test1","propertyType":"colorpicker","oldValue":"beforeEdit1","newValue":null},{"propertyName":"application.property.test2","propertyType":"togglebutton","oldValue":true,"newValue":null},{"propertyName":"application.property.test2","propertyType":"text","oldValue":"beforeEdit2","newValue":null},{"propertyName":"application.property.test3","propertyType":"text","oldValue":"beforeEdit3","newValue":null},{"propertyName":"application.property.test2","propertyType":"text","oldValue":"beforeEdit2","newValue":null},{"propertyName":"application.property.test3","propertyType":"text","oldValue":"beforeEdit3","newValue":null},{"propertyName":"application.property.test2","propertyType":"text","oldValue":"beforeEdit2","newValue":null},{"propertyName":"application.property.test3","propertyType":"text","oldValue":"beforeEdit3","newValue":null},{"propertyName":"application.property.test2","propertyType":"text","oldValue":"beforeEdit2","newValue":null},{"propertyName":"application.property.test3","propertyType":"text","oldValue":"beforeEdit3","newValue":null},{"propertyName":"application.property.test2","propertyType":"text","oldValue":"beforeEdit2","newValue":null},{"propertyName":"application.property.test3","propertyType":"text","oldValue":"beforeEdit3","newValue":null},{"propertyName":"application.property.test2","propertyType":"text","oldValue":"beforeEdit2","newValue":null},{"propertyName":"application.property.test3","propertyType":"text","oldValue":"beforeEdit3","newValue":null},{"propertyName":"application.property.test2","propertyType":"text","oldValue":"beforeEdit2","newValue":null},{"propertyName":"application.property.test3","propertyType":"text","oldValue":"beforeEdit3","newValue":null},{"propertyName":"application.property.test2","propertyType":"text","oldValue":"beforeEdit2","newValue":null},{"propertyName":"application.property.test3","propertyType":"text","oldValue":"beforeEdit3","newValue":null}]},{"fileName":"theme.css","properties":[{"propertyName":"css.property.test1","propertyType":"colorpicker","oldValue":"beforeEdit1","newValue":null},{"propertyName":"css.property.test2","propertyType":"url","oldValue":"beforeEdit2","newValue":null}]}]';
    // this.configs = JSON.parse(dummyJson);

    // Move this to a service class
    this.http.get<config[]>("http://localhost:8585/config/local/prod/master")
      .subscribe((response) => {
        // this.configProperties = response;
        this.configs = response;
        console.log(this.configs);
      });
  }

  callUpdateApi() {
    this.configs.forEach((config) => {
      var properties: Property[] = [];
      config.properties.forEach((property) => {
        if (property.newValue != null)
          properties.push(property);
      });
      if (properties.length > 0) {
        var newConfig: config = {
          "propertySource": config.propertySource,
          "application": config.application,
          "profile": config.profile,
          "properties": properties
        };
        this.updatedConfigs.push(newConfig);
      }
    });
    console.log(this.updatedConfigs);

    // Posting the updates

    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     'Content-Type': 'application/json'
    //   })
    // };
    // this.http.post("http://localhost:8585/config/configupdate", JSON.stringify(this.updatedConfigs), httpOptions)
    //   .subscribe(response => {
    //     console.log(response);
    //     // correct the logic so that httprequest will be successful
    //     this.updatedConfigs = [];
    //   });
    // this.updatedConfigs = [];
  }

}
