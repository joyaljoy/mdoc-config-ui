import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { JsonEditorOptions } from 'ang-jsoneditor';
import { config } from 'src/app/shared/models/config';
import { schema } from 'src/app/shared/schema.value';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.scss']
})
export class EditItemComponent implements OnInit {

  public disabled = false;
  public color: ThemePalette = 'primary';
  public touchUi = false;
  // colorCtr = new FormControl(null);
  colorCtr: FormControl[] = [];
  defaultColors: {propertyName: string, propertyColor: string}[] = [];

  // For material Angular drop down list
  toppings = new FormControl();
  toppingList: string[] = ['Property 1', 'Property 2', 'Property 3'];

  // File upload
  // @ViewChild('fileInput') fileInput: ElementRef;
  fileAttr = 'Choose File';


  ///////////////////////
  @Input() config: config;
  @Output() updatedConfigEvent = new EventEmitter<config>();
  // updatedConfig: config;

  filterInput: any = "";
  // dummy json
  jsonData: string;
  editorOptions = new JsonEditorOptions();

  constructor() {
    this.editorOptions.mode = 'tree';
    this.editorOptions.modes = ['code', 'text', 'tree', 'view'];
    this.editorOptions.schema = schema;
    // this.editorOptions.statusBar = false; 
  }

  ngOnInit(): void {
    // console.log("logging config: " + JSON.stringify(this.config));
    // console.log(this.config);
    // this.updatedConfig = {
    //   "propertySource": this.config.propertySource,
    //   "application": this.config.application,
    //   "profile": this.config.profile,
    //   "properties": [],
    // };
    // console.log("ngOnInit: " + this.jsonData);

    for (let i = 0; i < this.config.properties.length; i++) {
      this.colorCtr.push(new FormControl(null));
      // if(this.config.properties[i].propertyType == "COLOR_PICKER"){
      //   // this.colorCtr.push(new FormControl({ hex : this.config.properties[i].currentValue }));
      //   // this.colorCtr[i].value.hex = this.config.properties[i].currentValue;
      //   // this.colorCtr.push(new FormControl(''));
      //   this.defaultColors.push({"propertyName": this.config.properties[i].propertyName,"propertyColor": this.config.properties[i].currentValue});
      // } 
      // else {
      //   this.colorCtr.push(new FormControl(''));
      // }
    }

    var jsonObject = JSON.parse('{"PAGINATOR":{"OF":"of","ITEMS_PER_PAGE":"Items per page:","NEXT_PAGE":"Next page","PREVIOUS_PAGE":"Previous page"},"DATE":{"DAY_LABELS":{"su":"Sun","mo":"Mon","tu":"Tue","we":"Wed","th":"Thu","fr":"Fri","sa":"Sat"},"MONTH_LABELS":{"1":"Jan","2":"Feb","3":"Mar","4":"Apr","5":"May","6":"Jun","7":"Jul","8":"Aug","9":"Sep","10":"Oct","11":"Nov","12":"Dec"},"TODAY_BTN_TXT":"Today","MONTH":"Month","WEEK":"Week","DAY":"Day"},"GDPR":{"NOT_ACTIVATED":"Not activated","DAYS_LEFT_UNTIL_DELETION":" days until account deletion","DELETE_MESSAGE":" this account will be deleted if not activated by the patient","DELETE_MESSAGE_PR":"On ","DELETED":"Deleted"}}');
    this.jsonData = this.flattenJson(jsonObject);
  
  }

  getColor(propertyName: string){
    return this.defaultColors.find( property => 
      property.propertyName == propertyName
    )?.propertyColor;
  }

  printColor(colorPicker: any) {
    console.log("HEX value: " + colorPicker._selected.hex);
    console.log("RGBA value: " + colorPicker._selected.rgba);
  }

  uploadFileEvt(imgFile: any) {
    if (imgFile.target.files && imgFile.target.files[0]) {
      this.fileAttr = '';
      // Array.from(imgFile.target.files).forEach((file: File) => {
      //   this.fileAttr += file.name;
      // });
      this.fileAttr += imgFile.target.files[0].name;
      console.log("url: " + JSON.stringify(imgFile.target.files));

      // HTML5 FileReader API
      let reader = new FileReader();
      reader.onload = (e: any) => {
        let image = new Image();
        image.src = e.target.result;
        image.onload = rs => {
          let imgBase64Path = e.target.result;
        };
      };
      // reader.readAsDataURL(imgFile.target.files[0]);
      reader.readAsBinaryString(imgFile.target.files[0]);

      // Reset if duplicate image uploaded again
      // this.fileInput.nativeElement.value = "";
    } else {
      this.fileAttr = 'Choose File';
    }
  }

  updateNewValue(propertyName: string, event: any) {
    this.config.properties.forEach((property) => {
      if (property.propertyName == propertyName)
        property.newValue = event.target.value;
    }); 
    this.updatedConfigEvent.emit(this.config);
    // console.log(this.config);
  }


  // Json
  jsonDataChanges(updatedJson: any) {
    console.log(updatedJson);
    console.log(this.unflattJson(updatedJson))
  }

  // Flatten and Unflatten JSON
  flattenJson(data: any) {
    var result: any = {};
    function recurse(cur: any, prop: any) {
      if (Object(cur) !== cur) {
        result[prop] = cur;
      } else if (Array.isArray(cur)) {
        for (var i = 0, l = cur.length; i < l; i++)
          recurse(cur[i], prop + "[" + i + "]");
        if (l == 0)
          result[prop] = [];
      } else {
        var isEmpty = true;
        for (var p in cur) {
          isEmpty = false;
          recurse(cur[p], prop ? prop + "." + p : p);
        }
        if (isEmpty && prop)
          result[prop] = {};
      }
    }
    recurse(data, "");
    return result;
  }

  unflattJson(data: any) {
    "use strict";
    if (Object(data) !== data || Array.isArray(data))
      return data;
    var regex = /\.?([^.\[\]]+)|\[(\d+)\]/g,
      resultholder: any = {};
    for (var p in data) {
      var cur: any = resultholder,
        prop = "",
        m;
      while (m = regex.exec(p)) {
        cur = cur[prop] || (cur[prop] = (m[2] ? [] : {}));
        prop = m[2] || m[1];
      }
      cur[prop] = data[p];
    }
    return resultholder[""] || resultholder;
  };

}
