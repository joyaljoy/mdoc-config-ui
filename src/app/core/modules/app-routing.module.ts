import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigSelectComponent } from '../config-select/config-select.component';
import { EditItemListComponent } from '../edit-item-list/edit-item-list.component';
import { LoginComponent } from '../login/login.component';

const routes: Routes = [
  {
    path: '', component: LoginComponent,
    // path: '', canActivate: [AuthGuard], component: LoginComponent,
    // children: [
    //   {
    //     path: 'login', component: LoginComponent
    //   }
    // ]
  },
  {
    path: 'edit-properties', component: EditItemListComponent,
  },
  {
    path: 'config-select', component: ConfigSelectComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
