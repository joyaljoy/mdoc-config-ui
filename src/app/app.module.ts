import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './core/login/login.component';
import { EditItemListComponent } from './core/edit-item-list/edit-item-list.component';
import { EditItemComponent } from './core/edit-item/edit-item.component';
import { AngularMaterialModule } from './core/modules/angular-material.module';
import { AppRoutingModule } from './core/modules/app-routing.module';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MAT_COLOR_FORMATS, NgxMatColorPickerModule, NGX_MAT_COLOR_FORMATS } from '@angular-material-components/color-picker';
import { MatCardModule } from '@angular/material/card';
import { ConfigSelectComponent } from './core/config-select/config-select.component';
import { PropertyFilterPipe } from './shared/pipe/property-filter-pipe.pipe';
import { NgJsonEditorModule } from 'ang-jsoneditor';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EditItemListComponent,
    EditItemComponent,
    ConfigSelectComponent,
    PropertyFilterPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    AngularMaterialModule,
    // MatSlideToggleModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatCardModule,
    NgxMatColorPickerModule,
    NgJsonEditorModule
  ],
  providers: [{ provide: MAT_COLOR_FORMATS, useValue: NGX_MAT_COLOR_FORMATS }],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
